emoji = {'kiss': '😘',
         'joy': '😂',
         'heart': '❤',
         'heart_eye': '😍',
         'blush': '😊',
         'grin': '😁',
         'Plus1': '👍',
         'house': '🏠',
         'package': '📦',
         'car': '🚘',
         'bar_charts': '📊',
         'chart_with_upwards_trend': '📈',
         'chart_with_downwards_trend': '📉',
         'clipboard': '📋',
         'moneybag': '💰',
         'credit_card': '💳',
         'clock1': '🕐',
         'necktie': '👔',
         'memo': '📝',
         'information_source': 'ℹ',
         'ruble': '₽',
         'exclamation': '❗', "cop": "👮", "alien": "👽", "speech_balloon": "💬", }

emoji_keys_tuple = tuple(emoji.keys())

# from data import emoji, emoji_keys_tuple
# f = open('emoji_class.py', 'w')
# f.write('class Emoji:\n')
# for e in emoji_keys_tuple:
#     f.write(f"\t{e.upper()} = '{emoji[e]}'\n")
# f.close()
