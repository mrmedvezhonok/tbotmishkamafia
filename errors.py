class OrderMissingDataError(Exception):
    """
    Missing data in Order class.
    """
    pass


class UnknownOrderError(Exception):
    """
    Order from message not exists in bot storage.
    """
    pass
