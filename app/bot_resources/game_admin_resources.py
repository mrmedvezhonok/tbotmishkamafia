from app import bot
from app import Player
from app import GameAdmin

from telebot.types import Message

from emoji_class import Emoji


@bot.message_handler(commands=["admin_players"])
def bot_admin_command_players(msg: Message):
    from_user = msg.from_user
    chat_id = from_user.id
    # validate admin
    admin = GameAdmin.get_registered(chat_id)
    if admin is None:
        bot.send_message(chat_id, f"{Emoji.ALIEN} Я не понимаю о чём идёт речь.")
        return
    bot.send_chat_action(chat_id, 'typing')  # show the bot "typing" (max. 5 secs)
    # if admin registered admin ask
    players = Player.get_all_registered()
    reply_text = ""
    for player in players:
        reply_text += f"{str(player)}\n"
    bot.send_message(chat_id, reply_text)
