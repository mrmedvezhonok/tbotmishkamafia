from app import bot
from app import Player

from telebot.types import Message

from emoji_class import Emoji


# COMMANDS

@bot.message_handler(func=lambda m: m.chat.type == 'private', commands=['register'])
def bot_handler_command_register_private(msg: Message):
    from_user = msg.from_user
    chat_id = from_user.id

    # check is registered yet
    player = Player.get_registered(chat_id)
    if player is not None:
        bot.send_message(chat_id, f'Вы уже зарегистрированы {Emoji.EXCLAMATION}')
        return

    # Player.register(chat_id, username, first_name, last_name)
    # bot.send_message(chat_id, f'Вы успешно зарегистрированы {Emoji.Plus1}')
    bot.register_next_step_handler(msg, bot_register_step_username)
    bot.send_message(chat_id, f"{Emoji.MEMO} Давай для начала определим твой ник.")


def bot_register_step_username(msg: Message):
    # TODO продумать изменение никнейма

    from_user = msg.from_user
    chat_id = from_user.id
    first_name = from_user.first_name
    last_name = from_user.last_name
    telegram_username = from_user.username
    username = msg.text
    pl = Player.register(chat_id, username, first_name, last_name, telegram_username)
    bot.send_message(chat_id, f'Вы успешно зарегистрированы, {pl.username} {Emoji.PLUS1}.')


# @bot.message_handler()
# def bot_handler_text(msg: Message):
#     print(msg.text)
#     chat_id = msg.chat.id
#     reply_message = 'You are welcome!'
#     bot.send_message(chat_id, reply_message)
