from app import bot

from telebot.types import Message
from telebot.types import InlineKeyboardMarkup
from telebot.types import InlineKeyboardButton


@bot.message_handler(commands=['test_im'])
def bot_test_inm(msg: Message):
    keyboard = InlineKeyboardMarkup(3)
    buttons = list()
    buttons.append(InlineKeyboardButton("Мужчина", callback_data="male"))
    buttons.append(InlineKeyboardButton("Женщина", callback_data="female"))
    buttons.append(InlineKeyboardButton("Не скажу", callback_data="not_defined"))
    keyboard.add(*buttons)
    bot.send_message(msg.from_user.id, "Каков ваш пол?", reply_markup=keyboard)

@bot.message_handler()
def bot_handler_any_text(msg: Message):
    print(msg.text)
    bot.send_message(msg.from_user.id, msg.text)