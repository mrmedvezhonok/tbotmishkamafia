from app import db
from peewee import Model
from peewee import AutoField
from peewee import IntegerField
from peewee import CharField
from peewee import DateTimeField

from datetime import datetime as dt


class GameAdmin(Model):
    id = AutoField()

    chat_id = IntegerField(null=False, unique=True, index=True)
    telegram_username = CharField(null=True)
    registration_dt = DateTimeField(null=False, default=dt.utcnow())

    class Meta:
        database = db

    def __repr__(self):
        return f"<GameAdmin {self.chat_id}: {self.username} >"

    def __str__(self):
        return f"{self.chat_id}: {self.username}"

    @staticmethod
    def register(chat_id: int, telegram_username: str = None):
        player = GameAdmin(chat_id=chat_id, telegram_username=telegram_username)
        player.save()
        return player

    @staticmethod
    def get_registered(chat_id: int):
        # get registered Player object or None
        player = GameAdmin.select().where(GameAdmin.chat_id == chat_id)
        if player.exists():
            return player.get()
