class MafiaRole:
    role_name = ["Не в игре", "Гражданский", "Мафия", "Дон мафии", "Комиссар", "Доктор", "Куртизанка", "Маньяк"]
    min, max = 0, len(role_name)
    NOT_PLAYING = 0
    MAFIA = 1
    DON_MAFIA = 2
    COMMISSAR = 3
    DOCTOR = 4
    COURTESAN = 5
    MANIAK = 6

    @staticmethod
    def get_name(role):
        if MafiaRole.min <= role <= MafiaRole.max:
            return MafiaRole.role_name[role]

#
# class Gender:
#     name = ["Не определен", "Мужской", "Женский"]
#     min, max = 0, len(name)
#     NOT_DEFINED = 0
#     MALE = 1
#     FEMALE = 2
#
#     @staticmethod
#     def get_name(gender):
#         if Gender.min <= gender <= Gender.max:
#             return Gender.name[gender]
