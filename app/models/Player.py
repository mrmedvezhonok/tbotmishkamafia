from app import db
from peewee import Model
from peewee import AutoField
from peewee import IntegerField
from peewee import CharField
from peewee import DateTimeField

from datetime import datetime as dt

from app.models import MafiaRole

from emoji_class import Emoji


class Player(Model):
    id = AutoField()

    chat_id = IntegerField(null=False, unique=True, index=True)
    telegram_username = CharField(null=True)

    username = CharField(null=True)
    first_name = CharField(null=True)
    last_name = CharField(null=True)

    registration_dt = DateTimeField(null=False, default=dt.utcnow())
    role = 0

    class Meta:
        database = db

    def __repr__(self):
        return f"<Player {self.chat_id}: {self.username} " \
            f"{self.last_name} {self.first_name} {MafiaRole.get_name(self.role)}> "

    def __str__(self):
        return f"{self.chat_id}: {self.username} " \
            f"{Emoji.NECKTIE} {self.last_name} {self.first_name} {Emoji.COP} {MafiaRole.get_name(self.role)}"

    @staticmethod
    def register(chat_id: int, username: str, first_name: str = None, last_name: str = None,
                 telegram_username: str = None):
        player = Player(chat_id=chat_id, username=username, first_name=first_name, last_name=last_name,
                        telegram_username=telegram_username)
        player.save()
        return player

    @staticmethod
    def get_registered(chat_id: int):
        # get registered Player object or None
        player = Player.select().where(Player.chat_id == chat_id)
        if player.exists():
            return player.get()

    @staticmethod
    def get_all_registered():
        select = Player.select()
        if select.exists():
            players = [pl for pl in select]
            return players
