from telebot import TeleBot
from peewee import SqliteDatabase

from config import BOT_TOKEN
from config import DB_PATH

bot = TeleBot(BOT_TOKEN)

db = SqliteDatabase(DB_PATH)

from app.models.Player import Player
from app.models.GameAdmin import GameAdmin

Player.create_table()
GameAdmin.create_table()

from app.bot_resources import *
from app.bot_resources.game_admin_resources import *

if __name__ == "__main__":
    bot.polling(False, 2)
