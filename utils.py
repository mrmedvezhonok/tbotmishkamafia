
from datetime import timezone
from datetime import timedelta
from datetime import datetime as dt
from datetime import time


def dt_now(tz_offset=None):
    """
    Default using utc time zone. The function convert utc tz to tz_offset.
    :param tz_offset:
    :return: dt.utcnow with offset of time zone.
    """
    if tz_offset is None:
        # IF tz is None return utc.
        return dt.utcnow()
    if not isinstance(tz_offset, int):
        raise TypeError("tz_offset must be int")
    td = timedelta(hours=tz_offset)
    utcnow = dt.utcnow()
    return utcnow.replace(tzinfo=timezone.utc).astimezone(timezone(td))


def time_str2time(time_str: str):
    """
    Convert string contains hours and minutes time to datetime.time object
    :param time_str: str in format "hour:minute". Example  "18:45"
    :return:
    """
    if not isinstance(time_str, str):
        raise TypeError("time_str must be str.3")
    hours, minutes = tuple(map(int, time_str.split(':')))
    return time(hour=hours, minute=minutes)
